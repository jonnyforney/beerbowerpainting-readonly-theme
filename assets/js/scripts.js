var primary_email = "beerbowerpaintingllc@gmail.com";

$(document).ready(function() {
    //  load post images
    $.get(
        ghost.url.api('posts')
    ).done(function(data) {
        if (data.posts.length > 0) {
            $.each(data.posts, function(i) {
                if (this.featured) {
                    var post_html = "<article class='flex'>" +
                        "<div class=''>";

                    //  create title
                    var title = "<h4>" + this.title + "</h4>";
                    if (this.meta_title) {
                        title = "<h4>" + this.meta_title + "</h4>";
                    }
                    post_html += title;

                    //  find imgs
                    var imgs = $(this.html).find('img');
                    console.log(imgs);
                    post_html += "<div class='content flex-center'>";
                    $.each(imgs, function(i) {
                        console.log(this);
                        var img_container = "<div class='img-container'>" +
                            "<span>" + this.alt + "</span>" +
                            this.outerHTML +
                            "</div>";

                        post_html += img_container;
                    });
                    post_html += "</div>";

                    //  create description
                    var desc = "";
                    if (this.meta_description) {
                        desc = "<p class='flex description'>" + this.meta_description + "</p>";
                    }
                    post_html += desc;

                    //  close tags
                    post_html += "</div></article>";

                    $('#projects .features').append(post_html);
                }
            });
        } else {
            var post_html = '<p>There are currently no recent projects. Please check back later.</p>'
            $('#projects .features').append(post_html);
        }
    }).fail(function(err) {
        console.log(err);
        swal('Ops, there was an error.', err, 'error');
    });

    $('#contact-form').submit(function(e) {
        e.preventDefault();

        var name = $('#name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var preferred = $('input[name=prefer]:checked').val();
        var subject = $('#subject').val();
        var message = $('#message').val();

        var preferred_message = '\nPreferred Contact Method: ';
        switch (preferred) {
            case 'email':
                preferred_message += '\nEmail -> ' + email;
                break;
            case 'phone':
                preferred_message += '\nPhone -> ' + phone;
                break;
            case 'either':
                preferred_message += '\nEmail -> ' + email;
                preferred_message += '\nPhone -> ' + phone;
                break;
        }

        message += preferred_message;

        var data = {
            'name': name,
            '_replyto': email,
            'subject': subject,
            'message': message,
            '_gotcha': $('input[name=_gotcha]').val()
        };

        $.ajax({
            url: 'https://formspree.io/' + primary_email,
            method: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $('#contact-form').append('<div class="alert alert--loading">Sending message…</div>');
            },
            success: function(data) {
                $('#contact-form .alert--loading').hide();
                swal({
                    title: 'Message sent!',
                    text: '<b>Thank you for your interest! We will get back to you shortly!</b>',
                    type: 'success',
                    html: true
                });
                $('#contact-form').find("input[type=text], textarea, input[type=email]").val("");
            },
            error: function(err) {
                $('#contact-form .alert--loading').hide();
                swal('Ops, there was an error.', '', 'error');
            }
        });
    });

    $('#contact-form input[type=radio]').on('change', function() {
        switch ($('input[name=prefer]:checked').val()) {
            case 'email':
                $('#email').removeClass('hide');
                $('#phone').addClass('hide');
                break;
            case 'phone':
                $('#phone').removeClass('hide');
                $('#email').addClass('hide');
                break;
            case 'either':
                $('#phone').removeClass('hide');
                $('#email').removeClass('hide');
                break;
        }
    });

});

function focus_el(id) {
    $('#' + id).focus();
}