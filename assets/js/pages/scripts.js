$(document).ready(function() {
			$.ajax({
			method: 'GET',
			url: ghost.url.api('posts'),
			complete: function(xhr, textStatus) {
			},
			success: function(data, textStatus, xhr) {
        // console.log('posts', data.posts);

        var image_objs = data.posts.filter(function(obj) {
          return obj.html.includes('img');
        });

        var media = {
          'media': []
        };

        if(image_objs.length > 0) {
          $.each(image_objs, function(i) {
            //  find imgs
            var imgs = $(this.html).find('img');
            $.each(imgs, function(i) {
              var obj = {
                'url': $(this).attr('src'),
                'type':'image'
              }
							var img_html = "<div><img src='"+obj.url+"' class='cell' onclick='preview(this)' /></div>";
							$('#grid').append(img_html);
            });
          });
        } else {
          var gallery_html = '<p>The gallery is empty. Please check back later.</p>';
        }
			},
			error: function(xhr, textStatus, errorThrown) {
        swal('Ops, there was an error.',err,'error');
			}
		});

		$('.preview').on('click', function() {
			  $('.preview').addClass('hide');
				$('body').removeClass('no-scroll');
				$('html').removeClass('no-scroll');
			  $('.overlay').css('z-index','-1');
				$('.overlay').removeClass('darker');
		})
	});

	function preview(el) {
		var src = el.src;

		$('.preview img').attr('src',src);
		$('body').addClass('no-scroll');
		$('html').addClass('no-scroll');
		$('.preview').removeClass('hide');
		$('.overlay').css('z-index','0');
		$('.overlay').addClass('darker');
	}
